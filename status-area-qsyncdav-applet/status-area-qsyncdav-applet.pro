TEMPLATE = lib
CONFIG += shared
CONFIG -= qt

DEFINES += QSYNCDAEMON_DBUS_SERVICE="org.maemo.QSyncDAVDaemon"

# This library requires a hildon desktop dev environment !
maemo5 {
    DEFINES += MAEMO_CHANGES

    INCLUDEPATH += /usr/include/libhildondesktop-1 /usr/include/hildon-1
    INCLUDEPATH += /usr/include/gtk-2.0 /usr/lib/gtk-2.0/include /usr/include/gconf/2 /usr/include/gio-unix-2.0 /usr/include/cairo
    INCLUDEPATH += /usr/include/glib-2.0 /usr/lib/glib-2.0/include
    INCLUDEPATH += /usr/include/pango-1.0 /usr/include/atk-1.0
    INCLUDEPATH +=/usr/include/dbus-1.0 /usr/lib/dbus-1.0/include
    LIBS += -lhildon-1 -lhildondesktop-1 -lgtk-x11-2.0 -lgconf-2 -lglib-2.0 -lgio-2.0 -lcairo

    SOURCES += \
        src/lib-qsyncdav-status-menu-widget.c

    HEADERS += \
        src/lib-qsyncdav-status-menu-widget.h
}

OTHER_FILES += \
    data/status-area-qsyncdav-applet.desktop \
    data/qsyncdav.sync_running.png \
    data/qsyncdav.sync_complete.png

maemo5 {
    target.path = /usr/lib/hildon-desktop

    statusicon18.path = /usr/share/icons/hicolor/18x18/hildon
    statusicon18.files += data/qsyncdav.sync_complete.png data/qsyncdav.sync_running.png

    desktophildon.path = /usr/share/applications/hildon-status-menu
    desktophildon.files += data/status-area-qsyncdav-applet.desktop

    INSTALLS += target statusicon18 desktophildon
}
