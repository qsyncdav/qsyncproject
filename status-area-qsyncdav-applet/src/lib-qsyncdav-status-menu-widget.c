 /***********************************************************************************
 *  Orientation lock status area plugin
 *  Copyright (C) 2011 Mohammad Abu-Garbeyyeh
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 ***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <libintl.h>
#include <gtk/gtk.h>
#include <hildon/hildon.h>
#include <gconf/gconf-client.h>
#include <dbus/dbus-glib.h>

#include "lib-qsyncdav-status-menu-widget.h"

#define QSYNCDAV_STATUS_PLUGIN_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE (obj, \
                            TYPE_QSYNCDAV_STATUS_PLUGIN, QSyncDAVStatusPluginPrivate))

#define QSYNCDAV_DBUS_ADDRESS    "org.maemo.QSyncDAVDaemon"
#define QSYNCDAV_DBUS_OBJECT     "/QSyncDAVDaemon"
#define GCONF_KEY_QSYNCDAV       "/apps/osso/hildon-desktop/qsyncdav_status"

struct _QSyncDAVStatusPluginPrivate
{
    //GtkWidget *button;
    GConfClient *gconf_client;
    gpointer data;
};

DBusGConnection *pDBusConnection = NULL;
DBusGProxy *pQSyncDAVDaemoProxy = NULL;
DBusGProxy *pDBusItfProxy = NULL;
DBusConnection *pPrivateDBusConnection = NULL;

HD_DEFINE_PLUGIN_MODULE (QSyncDAVStatusPlugin, qsyncdav_status_plugin, HD_TYPE_STATUS_MENU_ITEM);

static void
qsyncdav_status_plugin_class_finalize (QSyncDAVStatusPluginClass *klass)
{
    g_object_unref(pQSyncDAVDaemoProxy);
    g_object_unref(pDBusItfProxy);
    dbus_g_connection_unref(pDBusConnection);
}

static void
qsyncdav_status_plugin_class_init (QSyncDAVStatusPluginClass *klass)
{
    g_type_class_add_private (klass, sizeof (QSyncDAVStatusPluginPrivate));
}

/* Connect to am dbus server
 * return 0 means failed
 * return 1 means succeeded
 * */
static gint dbus_init(QSyncDAVStatusPlugin *plugin)
{
    pDBusConnection = hd_status_plugin_item_get_dbus_g_connection(HD_STATUS_PLUGIN_ITEM (&plugin->parent), DBUS_BUS_SESSION, NULL);
    if (pDBusConnection == NULL) {
        return 0;
    }

    pDBusItfProxy = dbus_g_proxy_new_for_name(pDBusConnection, DBUS_SERVICE_DBUS, DBUS_PATH_DBUS, DBUS_INTERFACE_DBUS);

    if (pDBusItfProxy == NULL) {
        g_print("connect to freedesktop dbus server failed\n");
        return 0;
    }
    else {
        g_print("connect to freedesktop dbus server succeeded\n");
    }

    return 1;
}

#if 0
static void qsyncdav_status_plugin_on_status_change(GConfClient *client, guint cnxn_id, GConfEntry *entry, gpointer user_data)
{
    QSyncDAVStatusPlugin *plugin = (QSyncDAVStatusPlugin *)user_data;
    QSyncDAVStatusPluginPrivate *priv = QSYNCDAV_STATUS_PLUGIN_GET_PRIVATE (plugin);

    /*
     * 0: sync is complete
     * 1: sync is running
     */
    gint current_status = gconf_client_get_int(priv->gconf_client, GCONF_KEY_QSYNCDAV, NULL);
    const gchar *icon_name = "qsyncdav.sync_complete";
    if( current_status == 1 )
    {
        icon_name = "qsyncdav.sync_running";
    }

    GtkIconTheme *icon_theme = gtk_icon_theme_get_default ();
    GdkPixbuf *pixbuf = gtk_icon_theme_load_icon (icon_theme, icon_name,
                        18, GTK_ICON_LOOKUP_NO_SVG, NULL);
    hd_status_plugin_item_set_status_area_icon (HD_STATUS_PLUGIN_ITEM (plugin), pixbuf);
    g_object_unref (pixbuf);
}
#endif

static void qsyncdav_status_plugin_on_status_change(DBusGProxy *proxy, const gchar* accountName, guint32 updated_status, QSyncDAVStatusPlugin *plugin)
{
    /* updated_status:
     * 0: sync is complete
     * 1: sync is running
     */
    const gchar *icon_name = "qsyncdav.sync_complete";
    if( updated_status == 1 )
    {
        icon_name = "qsyncdav.sync_running";
    }

    GtkIconTheme *icon_theme = gtk_icon_theme_get_default ();
    GdkPixbuf *pixbuf = gtk_icon_theme_load_icon (icon_theme, icon_name,
                        18, GTK_ICON_LOOKUP_NO_SVG, NULL);
    hd_status_plugin_item_set_status_area_icon (HD_STATUS_PLUGIN_ITEM (plugin), pixbuf);
    g_object_unref (pixbuf);
}

static void dbus_NameOwnerChanged_CB(DBusGProxy* p, const gchar* serviceName, const gchar* oldOwner, const gchar* newOwner, QSyncDAVStatusPlugin *plugin) {

    if( g_strcmp0(serviceName, QSYNCDAV_DBUS_ADDRESS) == 0 )
    {
        if( newOwner && strlen(newOwner) > 0 )
        {
            // Daemon has subscribed to DBUS
            pPrivateDBusConnection = hd_status_plugin_item_get_dbus_connection(HD_STATUS_PLUGIN_ITEM (&plugin->parent), DBUS_BUS_SESSION, NULL);
            if( pDBusConnection )
            {
                // unref any previous proxy
                if( pQSyncDAVDaemoProxy )
                {
                    g_object_unref(pQSyncDAVDaemoProxy); pQSyncDAVDaemoProxy = NULL;
                }

                dbus_bus_add_match(pPrivateDBusConnection, "type='signal',path='/QSyncDAVDaemon',interface='org.maemo.QSyncDAVDaemon',member='statusChanged'", NULL);
                dbus_connection_add_filter(pPrivateDBusConnection, (DBusHandleMessageFunction)(qsyncdav_status_plugin_on_status_change), plugin, NULL);

                pQSyncDAVDaemoProxy = dbus_g_proxy_new_for_name_owner(pDBusConnection, QSYNCDAV_DBUS_ADDRESS, QSYNCDAV_DBUS_OBJECT, QSYNCDAV_DBUS_ADDRESS, NULL);
                if( pQSyncDAVDaemoProxy )
                {
                    dbus_g_proxy_add_signal(pQSyncDAVDaemoProxy, "statusChanged", G_TYPE_STRING, G_TYPE_UINT, G_TYPE_INVALID);
                    dbus_g_proxy_connect_signal(pQSyncDAVDaemoProxy, "statusChanged", G_CALLBACK (qsyncdav_status_plugin_on_status_change), plugin, NULL);
                }
            }
        }
        else
        {
            // Daemon has unsubscribed from DBUS
            if( pQSyncDAVDaemoProxy )
            {
                g_object_unref(pQSyncDAVDaemoProxy); pQSyncDAVDaemoProxy = NULL;
            }
        }
    }
}

static void
qsyncdav_status_plugin_init (QSyncDAVStatusPlugin *plugin)
{
    GError *error = NULL;

    plugin->priv = QSYNCDAV_STATUS_PLUGIN_GET_PRIVATE (plugin);

    plugin->priv->gconf_client = gconf_client_get_default();
    g_assert(GCONF_IS_CLIENT(plugin->priv->gconf_client));

    GtkIconTheme *icon_theme = gtk_icon_theme_get_default ();
    GdkPixbuf *pixbuf = gtk_icon_theme_load_icon (icon_theme, "qsyncdav.sync_complete",
                        STATUS_AREA_QSYNCDAV_ICON_SIZE, GTK_ICON_LOOKUP_NO_SVG, NULL);
    hd_status_plugin_item_set_status_area_icon (HD_STATUS_PLUGIN_ITEM (plugin), pixbuf);
    g_object_unref (pixbuf);

    // put a callback on DBus:
    //   * one callback to be able to detect when the Daemon registers itself
    //   * one callback to be able to detect when the Daemon changes its status
    if( dbus_init(plugin) && NULL != pDBusConnection && NULL != pDBusItfProxy )
    {
        dbus_g_proxy_add_signal(pDBusItfProxy, "NameOwnerChanged", G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INVALID);
        dbus_g_proxy_connect_signal(pDBusItfProxy, "NameOwnerChanged", G_CALLBACK(dbus_NameOwnerChanged_CB), plugin, NULL);
    }

    if( pDBusConnection )
    {
        pQSyncDAVDaemoProxy = dbus_g_proxy_new_for_name_owner(pDBusConnection, QSYNCDAV_DBUS_ADDRESS, QSYNCDAV_DBUS_OBJECT, QSYNCDAV_DBUS_ADDRESS, NULL);
        //pQSyncDAVDaemoProxy = dbus_g_proxy_new_for_peer(pDBusConnection, QSYNCDAV_DBUS_OBJECT, QSYNCDAV_DBUS_ADDRESS);

        if( pQSyncDAVDaemoProxy )
        {
            fclose(fopen("/tmp/qsyncdav.QSyncDAVDaemoProxy", "a"));

            dbus_g_proxy_add_signal(pQSyncDAVDaemoProxy, "statusChanged", G_TYPE_STRING, G_TYPE_UINT, G_TYPE_INVALID);
            dbus_g_proxy_connect_signal(pQSyncDAVDaemoProxy, "statusChanged", G_CALLBACK (qsyncdav_status_plugin_on_status_change), plugin, NULL);
        }
    }

    gtk_widget_show (GTK_WIDGET (plugin));
}
