/******************************************************************************
 *    Copyright 2012 Christophe Chapuis chris.chapuis@gmail.com
 *
 *    This file is part of QSyncDAV.
 *
 *    QSyncDAV is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    QSyncDAV is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with QSyncDAV.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

#include "QSyncDAVDaemonApp.h"
#include "QSyncDAVDaemonDBusAdaptor.h"

#include <QDBusConnection>

#include <QDebug>

int main(int argc, char *argv[])
{
    QSyncDAVDaemonApp app(argc, argv);

    // create the MainApplication adaptor:
    new QSyncDAVDaemonDBusAdaptor(&app);

    QDBusConnection connection = QDBusConnection::sessionBus();
    if( !connection.isConnected() )
    {
        qDebug() << "[QSyncDAVDaemonApp] sessionBus is NOT connected !" << endl;
    }
    else
    {
        qDebug() << "[QSyncDAVDaemonApp] sessionBus is connected." << endl;
        bool ret = connection.registerService(QSYNCDAEMON_DBUS_SERVICE);
        if( ret )
        {
            qDebug() << "[QSyncDAVDaemonApp] " << QSYNCDAEMON_DBUS_SERVICE << " has been registered !" << endl;
            ret = connection.registerObject("/QSyncDAVDaemon", &app);
        }
    }

    app.reloadConfiguration();

    return app.exec();
}
