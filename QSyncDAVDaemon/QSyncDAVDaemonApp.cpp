#include "QSyncDAVDaemonApp.h"

#include <QSettings>
#include <QStringList>
#include <QList>
#include <QDebug>
#include <QDateTime>

#include "WebDAVDirSync.h"

#include "../QSyncDAVSettings/crypt_util.cpp"

QSyncDAVDaemonApp::QSyncDAVDaemonApp(int &argc, char **argv) :
    QCoreApplication(argc, argv),
    _logFile("/var/log/qsyncdavdaemon.log" )
{
    _logFile.open( QIODevice::ReadWrite | QIODevice::Text );
}

QSyncDAVDaemonApp::~QSyncDAVDaemonApp()
{
    _destroyAllSynchronizers();
    _logFile.close();
}

void QSyncDAVDaemonApp::logMessage(QString message)
{
    if( _logFile.isOpen() )
    {
        QTextStream logStream(&_logFile);
        logStream << "[" << QDateTime::currentDateTime().toString(Qt::ISODate) << "] "
                  << message << endl;
    }
}

void QSyncDAVDaemonApp::_destroyAllSynchronizers()
{
    for (int i = 0; i < _listSynchronizers.size(); ++i) {
        WebDAVDirSync *pAccountync = _listSynchronizers.at(i);
        if( pAccountync )
        {
            pAccountync->setEnabled(false);
            delete pAccountync; pAccountync = NULL;
        }
    }
    _listSynchronizers.clear();
}

void QSyncDAVDaemonApp::start(const QString &iAccountName)
{
    // Start all the accounts, or only <iAccountName> if not empty.
    for (int i = 0; i < _listSynchronizers.size(); ++i) {
        WebDAVDirSync *pAccountync = _listSynchronizers.at(i);
        if( pAccountync )
        {
            if( iAccountName.isEmpty() || pAccountync->getName() == iAccountName )
            {
                pAccountync->setEnabled(true);
            }
        }
    }
}

void QSyncDAVDaemonApp::deleteAccount(const QString &iAccountName)
{
    // Delete the account (i.e., stop it and delete its database file)
    for (int i = 0; i < _listSynchronizers.size(); ++i) {
        WebDAVDirSync *pAccountync = _listSynchronizers.at(i);
        if( pAccountync )
        {
            if( pAccountync->getName() == iAccountName )
            {
                pAccountync->setEnabled(false);
                pAccountync->deleteAccount();

                break;
            }
        }
    }
}

void QSyncDAVDaemonApp::stop(const QString &iAccountName)
{
    // Stop all the accounts, or only <iAccountName> if not empty.
    for (int i = 0; i < _listSynchronizers.size(); ++i) {
        WebDAVDirSync *pAccountync = _listSynchronizers.at(i);
        if( pAccountync )
        {
            if( iAccountName.isEmpty() || pAccountync->getName() == iAccountName )
            {
                pAccountync->setEnabled(false);
            }
        }
    }
}

void QSyncDAVDaemonApp::readyToSync(WebDAVDirSync *pAccountSync)
{
    if( pAccountSync )
    {
        if( pAccountSync->isEnabled() )
        {
            qDebug() << "statusChanged: starting sync" ;
            emit statusChanged(pAccountSync->getName(), 1); // sync is now running

            pAccountSync->sync();
        }
        else
        {
            qDebug() << "statusChanged: stopped" ;
            emit statusChanged(pAccountSync->getName(), 2); // sync is now stopped
        }
    }
}

void QSyncDAVDaemonApp::finishedSync(WebDAVDirSync *pAccountSync)
{
    // TODO: do a per-account status. If one account isn't finished, then the sync isn't complete...

    qDebug() << "statusChanged: complete" ;
    emit statusChanged(pAccountSync->getName(), 0); // sync is now complete

    bool bSyncCompleteForAll = true;
    for (int i = 0; i < _listSynchronizers.size() && bSyncCompleteForAll; ++i) {
        WebDAVDirSync *pAccountync = _listSynchronizers.at(i);
        if( pAccountync && pAccountync->isEnabled() )
        {
            if( pAccountSync->isBusy() )
                bSyncCompleteForAll = false;
        }
    }
    if( bSyncCompleteForAll )
    {
        emit statusChanged("", 0); // sync is complete for all accounts
    }
}

void QSyncDAVDaemonApp::reloadConfiguration()
{
    int i = 0;

    // First, clean up everything
    _destroyAllSynchronizers();

    QString server;
    QString login;
    QString password;
    QString localDir;
    QString remoteDir;
    qint64  syncInterval = 10;

    QStringList listAccounts;
    int nbAccounts = 0;

    QSettings settings("qsyncdav", "QSyncDAV");
    settings.beginGroup("GlobalSettings");
        listAccounts = settings.value("Accounts").toStringList();
    settings.endGroup();

    nbAccounts = listAccounts.size();
    for (i = 0; i < nbAccounts; ++i) {
        settings.beginGroup(listAccounts.at(i));
            server = settings.value("Server").toString();
            login = settings.value("Login").toString();
            DES_decrypt_string(settings.value("Password").toString(), password);
            localDir = settings.value("LocalDir").toString();
            remoteDir = settings.value("RemoteDir").toString();
            syncInterval = settings.value("SyncInterval").toInt();
            if( syncInterval < 1 ) syncInterval = 10; // failsafe
        settings.endGroup();

        WebDAVDirSync *pNewSync = new WebDAVDirSync(listAccounts.at(i));
        pNewSync->initialize(server, login, password,
                             remoteDir, localDir, syncInterval*60);
        pNewSync->setEnabled(true);

        // as soon as it is ready to sync, sync
        connect(pNewSync, SIGNAL(readyToSync(WebDAVDirSync*)), SLOT(readyToSync(WebDAVDirSync*)));
        connect(pNewSync, SIGNAL(finishedSync(WebDAVDirSync*)), SLOT(finishedSync(WebDAVDirSync*)));
        connect(pNewSync, SIGNAL(toLog(QString)), SLOT(logMessage(QString)));

        _listSynchronizers.append(pNewSync);
    }
}

