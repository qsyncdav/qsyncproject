#include "QSyncDAVDaemonDBusAdaptor.h"

#include <QString>
#include <QDBusAbstractAdaptor>

#include "QSyncDAVDaemonApp.h"

QSyncDAVDaemonDBusAdaptor::QSyncDAVDaemonDBusAdaptor(QSyncDAVDaemonApp *application) :
    QDBusAbstractAdaptor(application), app(application)
{
    connect(app, SIGNAL(statusChanged(QString, uint)), SIGNAL(statusChanged(QString, uint)));
}

void QSyncDAVDaemonDBusAdaptor::start(const QString &iAccountName)
{
    app->start(iAccountName);
}

void QSyncDAVDaemonDBusAdaptor::stop(const QString &iAccountName)
{
    app->stop(iAccountName);
}

void QSyncDAVDaemonDBusAdaptor::deleteAccount(const QString &iAccountName)
{
    if( !iAccountName.isEmpty() )
    {
        app->stop(iAccountName);
        app->deleteAccount(iAccountName);
    }
}

void QSyncDAVDaemonDBusAdaptor::reloadConfiguration()
{
    app->reloadConfiguration();
}

void QSyncDAVDaemonDBusAdaptor::quit()
{
    app->stop("");
    app->quit();
}
