QT -= gui # it is a daemon, without any UI element
QT += core network xml sql dbus

# If your application uses the Qt Mobility libraries, uncomment
# the following lines and add the respective components to the 
# MOBILITY variable. 
# CONFIG += mobility
# MOBILITY +=

CONFIG += qdbus

DEFINES += QSYNCDAEMON_DBUS_SERVICE=\'\"org.maemo.QSyncDAVDaemon\"\'

# Unix based platforms
unix:!symbian {

    LIBS += -lsqlite3

    maemo5 {
        # Maemo 5 specific
        message(Maemo 5 build)

        DEFINES += QSYNCDAV_CACHE_DIRECTORY=\'\"/home/user/.cache/qsyncdav\"\'
        DEFINES += QSYNCDAV_CONFIG_DIRECTORY=\'\"/home/user/.config/qsyncdav\"\'
    } else {
        contains(DEFINES, DESKTOP) {
            # Unix based desktop specific
            message(Unix based desktop build)

            DEFINES += QSYNCDAV_NO_SQLITE3_BACKUP
        }
        else {
            # Harmattan specific
            message(Harmattan build)

            DEFINES += QSYNCDAV_CACHE_DIRECTORY=\'\"/home/user/.cache/qsyncdav\"\'
            DEFINES += QSYNCDAV_CONFIG_DIRECTORY=\'\"/home/user/.config/qsyncdav\"\'
        }
    }
}


LIBS += -lcrypt

SOURCES += main.cpp \
    QWebDAV.cpp \
    WebDAVDirSync.cpp \
    sqlite3_util.cpp \
    QSyncDAVDaemonApp.cpp \
    QSyncDAVDaemonDBusAdaptor.cpp
HEADERS += \
    QWebDAV.h \
    WebDAVDirSync.h \
    sqlite3_util.h \
    QSyncDAVDaemonApp.h \
    QSyncDAVDaemonDBusAdaptor.h

maemo5 {
    target.path = /opt/QSyncDAVDaemon/bin
    INSTALLS += target

    service.path = $$DATADIR/dbus-1/services
    service.files += ../data/org.maemo.QSyncDAVDaemon.service
}
