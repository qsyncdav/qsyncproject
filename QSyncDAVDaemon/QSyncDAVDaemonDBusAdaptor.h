#ifndef QSYNCDAVDAEMONDBUSADAPTOR_H
#define QSYNCDAVDAEMONDBUSADAPTOR_H

#include <QDBusAbstractAdaptor>
#include <QString>

class QSyncDAVDaemonApp;

class QSyncDAVDaemonDBusAdaptor : public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.maemo.QSyncDAVDaemon")

private:
    QSyncDAVDaemonApp *app;

public:
    explicit QSyncDAVDaemonDBusAdaptor(QSyncDAVDaemonApp *application);

signals:
    void statusChanged(QString iAccountName, uint updatedStatus);
    
public Q_SLOTS:
    // start and stop services.
    // if iAccountName is empty, all accounts will be started/stopped.
    void start(const QString &iAccountName);
    void stop(const QString &iAccountName);
    // delete iAccountName. The name here must be non-empty.
    void deleteAccount(const QString &iAccountName);
    // reload the configuration to apply external changes
    void reloadConfiguration();

    // tell the daemon to exit
    Q_NOREPLY void quit();


};

#endif // QSYNCDAVDAEMONDBUSADAPTOR_H
