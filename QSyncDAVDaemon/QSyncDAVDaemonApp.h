#ifndef QSYNCDAVDAEMONAPP_H
#define QSYNCDAVDAEMONAPP_H

#include <QCoreApplication>
#include <QList>
#include <QFile>

#include "WebDAVDirSync.h"

class QTextStream;

class QSyncDAVDaemonApp : public QCoreApplication
{
    Q_OBJECT
public:
    explicit QSyncDAVDaemonApp(int &argc, char **argv);
    virtual ~QSyncDAVDaemonApp();
    
signals:
    void statusChanged(QString iAccountName, uint updatedStatus);
    
public slots:
    // start and stop services.
    // if iAccountName is empty, all accounts will be started/stopped.
    void start(const QString &iAccountName);
    void stop(const QString &iAccountName);
    void deleteAccount(const QString &iAccountName);

    // status changes
    void readyToSync(WebDAVDirSync*);
    void finishedSync(WebDAVDirSync*);

    // reload the configuration. This implies that all accounts are stopped, and restarted later on.
    void reloadConfiguration();

    // write to log
    void logMessage(QString message);

private:
    void _destroyAllSynchronizers();

    QFile _logFile;
    QList<WebDAVDirSync *> _listSynchronizers;
};

#endif // QSYNCDAVDAEMONAPP_H
