TEMPLATE = subdirs

SUBDIRS += status-area-qsyncdav-applet \
           QSyncDAVDaemon \
           QSyncDAVSettings

OTHER_FILES += \
    COPYING-GPL.txt \
    TODO.txt \
    qtc_packaging/debian_fremantle/rules \
    qtc_packaging/debian_fremantle/README \
    qtc_packaging/debian_fremantle/copyright \
    qtc_packaging/debian_fremantle/control \
    qtc_packaging/debian_fremantle/install \
    qtc_packaging/debian_fremantle/compat \
    qtc_packaging/debian_fremantle/changelog
