#include "AccountFrame.h"
#include "ui_AccountFrame.h"

#include <QFileDialog>
#include <QMessageBox>

#include <QDebug>

AccountFrame::AccountFrame(QWidget *parent, const QString &iAccountName) :
    QWidget(parent),
    ui(new Ui::AccountFrame),
    mAccountIsStarted(false)
{
    ui->setupUi(this);
    ui->accountGroupBox->setTitle(iAccountName);
}

AccountFrame::~AccountFrame()
{
    delete ui;
}

void AccountFrame::getAccountProperties(AccountFrame::AccountProperties &oProperties)
{
    oProperties.accountName = ui->accountGroupBox->title();
    oProperties.server = ui->serverLineEdit->text();
    oProperties.login = ui->loginLineEdit->text();
    oProperties.password = ui->passwordLineEdit->text();
    oProperties.remoteDir = ui->remoteDirLineEdit->text();
    oProperties.localDir = ui->localDirPushButton->text();
    oProperties.syncInterval = ui->syncIntervalComboBox->currentText().toInt();
}

void AccountFrame::setAccountProperties(const AccountFrame::AccountProperties &iProperties)
{
    ui->accountGroupBox->setTitle(iProperties.accountName);
    ui->serverLineEdit->setText(iProperties.server);
    ui->loginLineEdit->setText(iProperties.login);
    ui->passwordLineEdit->setText(iProperties.password);
    ui->remoteDirLineEdit->setText(iProperties.remoteDir);
    ui->localDirPushButton->setText(iProperties.localDir);
    ui->syncIntervalComboBox->lineEdit()->setText(QString::number(iProperties.syncInterval));
}

void AccountFrame::startstopAccount()
{
    ui->startstopPushButton->setDisabled(true);
    if( mAccountIsStarted )
        emit stop(ui->accountGroupBox->title());
    else
        emit start(ui->accountGroupBox->title());
}

void AccountFrame::setAccountIsStarted( bool isStarted )
{
    mAccountIsStarted = isStarted;

    ui->startstopPushButton->setText(isStarted?"Stop":"Start");
    ui->statusLabel->setText(isStarted?"synced":"disabled");
    ui->startstopPushButton->setDisabled(false);
}

void AccountFrame::destroyAccount()
{
    QMessageBox confirmationMsgBox;
    confirmationMsgBox.setText(QString("Delete account %1").arg(ui->accountGroupBox->title()));
    confirmationMsgBox.setInformativeText("Are you sure you want to delete this account?");
    confirmationMsgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    confirmationMsgBox.setDefaultButton(QMessageBox::Cancel);
    int ret = confirmationMsgBox.exec();

    if( ret == QMessageBox::Yes )
    {
        emit requestDeletion(this);
    }
}

void AccountFrame::onAccountStatusChange(const QString &iAccountName, uint newStatus)
{
    qDebug() << "onAccountStatusChange called for account: " << iAccountName << " with status " << newStatus << endl;
    if( iAccountName.isEmpty() || ui->accountGroupBox->title() == iAccountName )
    {
        // we are concerned
        if( 0 == newStatus ) // sync complete
        {
            setAccountIsStarted( true );
            ui->statusLabel->setText("synced");
        }
        else if( 1 == newStatus ) // sync running
        {
            setAccountIsStarted( true );
            ui->statusLabel->setText("syncing");
        }
        else if( 2 == newStatus ) // sync running
        {
            setAccountIsStarted( false );
            ui->statusLabel->setText("disabled");
        }
        else if( 3 == newStatus ) // sync running
        {
            setAccountIsStarted( false );
            ui->statusLabel->setText("N/A");
            ui->startstopPushButton->setDisabled(true);
        }
    }
}

void AccountFrame::showFileDialog()
{
    QString localDir = QFileDialog::getExistingDirectory(this, tr("Local Directory"), "/home/user/MyDocs");
    if (!localDir.isEmpty())
    {
        ui->localDirPushButton->setText(localDir);
    }
}
