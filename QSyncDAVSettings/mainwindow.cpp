/******************************************************************************
 *    Copyright 2012 Christophe Chapuis chris.chapuis@gmail.com
 *    Copyright 2011 Juan Carlos Cornejo jc2@paintblack.com
 *
 *    This file is part of QSyncDAV.
 *
 *    QSyncDAV is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    QSyncDAV is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with QSyncDAV.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "AccountFrame.h"

#include <QCoreApplication>
#include <QFileSystemWatcher>
#include <QTextStream>
#include <QFileDialog>
#include <QSettings>
#include <QInputDialog>
#include <QDBusInterface>
#include <QDBusConnectionInterface>
#include <QDBusServiceWatcher>

#include "crypt_util.cpp"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow),
    mLogFilePath( "/var/log/qsyncdavdaemon.log" ),
    mDbusDaemonItf(NULL),
    mLogWatcher(NULL)
{
    ui->setupUi(this);
    ui->daemonStartStopPushButton->setVisible(false);

    // DBus: be notified when a service is registered/unregistered
    QDBusServiceWatcher *watcher = new QDBusServiceWatcher( QSYNCDAEMON_DBUS_SERVICE, QDBusConnection::sessionBus(), QDBusServiceWatcher::WatchForOwnerChange, this );
    connect( watcher, SIGNAL(serviceRegistered (QString)), this, SLOT(onDBusServiceRegistered(QString)) );
    connect( watcher, SIGNAL(serviceUnregistered (QString)), this, SLOT(onDBusServiceUnregistered(QString)) );

    // Check if the service is already available
    QDBusConnectionInterface * pDBusConnectionItf = QDBusConnection::sessionBus().interface();
    if( pDBusConnectionItf )
    {
        QDBusReply<bool> lDBusBoolReply = pDBusConnectionItf->isServiceRegistered( QSYNCDAEMON_DBUS_SERVICE );
        if( lDBusBoolReply.isValid() && true == lDBusBoolReply.value() )
        {
            onDBusServiceRegistered( QSYNCDAEMON_DBUS_SERVICE );
        }
    }

    mLogWatcher = new QFileSystemWatcher( this );

    mLogWatcher->addPath( mLogFilePath );
    connect( mLogWatcher, SIGNAL( fileChanged ( const QString& ) ),
             this, SLOT( logFileChanged( const QString & ) ) );

    logFileChanged( mLogFilePath ); // initialize the UI
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setOrientation(ScreenOrientation orientation)
{
#if defined(Q_OS_SYMBIAN)
    // If the version of Qt on the device is < 4.7.2, that attribute won't work
    if (orientation != ScreenOrientationAuto) {
        const QStringList v = QString::fromLatin1(qVersion()).split(QLatin1Char('.'));
        if (v.count() == 3 && (v.at(0).toInt() << 16 | v.at(1).toInt() << 8 | v.at(2).toInt()) < 0x040702) {
            qWarning("Screen orientation locking only supported with Qt 4.7.2 and above");
            return;
        }
    }
#endif // Q_OS_SYMBIAN

    Qt::WidgetAttribute attribute;
    switch (orientation) {
#if QT_VERSION < 0x040702
    // Qt < 4.7.2 does not yet have the Qt::WA_*Orientation attributes
    case ScreenOrientationLockPortrait:
        attribute = static_cast<Qt::WidgetAttribute>(128);
        break;
    case ScreenOrientationLockLandscape:
        attribute = static_cast<Qt::WidgetAttribute>(129);
        break;
    default:
    case ScreenOrientationAuto:
        attribute = static_cast<Qt::WidgetAttribute>(130);
        break;
#else // QT_VERSION < 0x040702
    case ScreenOrientationLockPortrait:
        attribute = Qt::WA_LockPortraitOrientation;
        break;
    case ScreenOrientationLockLandscape:
        attribute = Qt::WA_LockLandscapeOrientation;
        break;
    default:
    case ScreenOrientationAuto:
        attribute = Qt::WA_AutoOrientation;
        break;
#endif // QT_VERSION < 0x040702
    };
    setAttribute(attribute, true);
}

void MainWindow::showExpanded()
{
#if defined(Q_OS_SYMBIAN) || defined(Q_WS_SIMULATOR)
    showFullScreen();
#elif defined(Q_WS_MAEMO_5)
    showMaximized();
#else
    show();
#endif
}

void MainWindow::addAccount()
{
    AccountFrame::AccountProperties lEmptyProperties;
    _addAccount(lEmptyProperties);
}

void MainWindow::_addAccount(AccountFrame::AccountProperties iProperties)
{
    bool ok = !iProperties.accountName.isEmpty();
    if( !ok )
    {
        iProperties.accountName = QInputDialog::getText(this, tr("New account"),
                                             tr("Account name : "), QLineEdit::Normal,
                                             "New Account", &ok);
    }

    if (ok && !iProperties.accountName.isEmpty())
    {
        AccountFrame *pNewAccount = new AccountFrame(ui->accountTabScrollArea, iProperties.accountName);
        pNewAccount->setAccountProperties(iProperties);
        ui->accountTabVerticalLayoutScroll->addWidget(pNewAccount);

        connect( pNewAccount, SIGNAL(start(QString)), SLOT(startAccount(QString)) );
        connect( pNewAccount, SIGNAL(stop(QString)), SLOT(stopAccount(QString)) );
        connect( pNewAccount, SIGNAL(requestDeletion(AccountFrame*)), SLOT(deleteAccount(AccountFrame*)) );
        connect( this, SIGNAL(accountStatusChanged(QString, uint)), pNewAccount, SLOT(onAccountStatusChange(QString, uint)));
    }
}

void MainWindow::deleteAccount(AccountFrame *ipAccount)
{
    if( ipAccount && mDbusDaemonItf )
    {
        // Tell the daemon that this account is to be deleted
        AccountFrame::AccountProperties lAccountProperties;
        ipAccount->getAccountProperties(lAccountProperties);
        mDbusDaemonItf->call( "deleteAccount", lAccountProperties.accountName );

        delete ipAccount; ipAccount = NULL;
    }
}

void MainWindow::saveConfiguration()
{
    QSettings settings("qsyncdav", "QSyncDAV");

    QStringList listAccounts;
    int i = 0;

    // Set it now, so that it appears at the beginning of the file.
    settings.setValue("GlobalSettings/Accounts", listAccounts);

    for( i = 0; i < ui->accountTabVerticalLayoutScroll->count(); i++ )
    {
        AccountFrame *pAccountFrame = qobject_cast<AccountFrame*>(ui->accountTabVerticalLayoutScroll->itemAt(i)->widget());
        if( pAccountFrame )
        {
            AccountFrame::AccountProperties lProperties;
            pAccountFrame->getAccountProperties(lProperties);

            listAccounts.append(lProperties.accountName);
            settings.beginGroup(lProperties.accountName);
                settings.setValue("Server", lProperties.server);
                settings.setValue("Login", lProperties.login);

                QString lEncryptedPwd;
                DES_encrypt_string(lProperties.password, lEncryptedPwd); // That's better than nothing

                settings.setValue("Password", lEncryptedPwd);
                settings.setValue("LocalDir", lProperties.localDir);
                settings.setValue("RemoteDir", lProperties.remoteDir);
                settings.setValue("SyncInterval", lProperties.syncInterval);
            settings.endGroup();
        }
    }

    settings.setValue("GlobalSettings/Accounts", listAccounts);

    settings.sync();

    // now tell the daemon to reload the configuration
    if( mDbusDaemonItf )
        mDbusDaemonItf->call( "reloadConfiguration" );
}

void MainWindow::loadConfiguration()
{
    QStringList listAccounts;
    int nbAccounts = 0, i = 0;

    QSettings settings("qsyncdav", "QSyncDAV");
    settings.beginGroup("GlobalSettings");
        listAccounts = settings.value("Accounts").toStringList();
    settings.endGroup();

    nbAccounts = listAccounts.size();
    for (i = 0; i < nbAccounts; ++i) {
        AccountFrame::AccountProperties lProperties;
        lProperties.accountName = listAccounts.at(i);
        settings.beginGroup(lProperties.accountName);
            lProperties.server = settings.value("Server").toString();
            lProperties.login = settings.value("Login").toString();
            DES_decrypt_string(settings.value("Password").toString(), lProperties.password);
            lProperties.localDir = settings.value("LocalDir").toString();
            lProperties.remoteDir = settings.value("RemoteDir").toString();
            lProperties.syncInterval = settings.value("SyncInterval").toInt();
        settings.endGroup();

        // Now that we have all the informations, create the new account
        _addAccount(lProperties);
    }
}

void MainWindow::on_actionSave_settings_triggered()
{
    saveConfiguration();
}


void MainWindow::on_actionReload_settings_triggered()
{
    loadConfiguration();
}

void MainWindow::startAccount(QString iAccountName)
{
    // Tell the daemon to start this account
    if( mDbusDaemonItf )
        mDbusDaemonItf->call( "start", iAccountName );
}

void MainWindow::stopAccount(QString iAccountName)
{
    // Tell the daemon to stop this account
    if( mDbusDaemonItf )
        mDbusDaemonItf->call( "stop", iAccountName );
}

void MainWindow::onDBusServiceRegistered(QString iServiceName )
{
    Q_UNUSED(iServiceName);

    // The daemon has just been registered, so (re)create the proxy
    if( mDbusDaemonItf ) {
        delete mDbusDaemonItf; mDbusDaemonItf = NULL;
    }
    mDbusDaemonItf = new QDBusInterface(QSYNCDAEMON_DBUS_SERVICE, "/QSyncDAVDaemon" );
    if( !mDbusDaemonItf->isValid() ) {
        delete mDbusDaemonItf; mDbusDaemonItf = NULL;
    }
    else
    {
        // update the UI to reflect the fact that the daemon is up and running
        ui->daemonStatusText->setText("Running");

        connect( mDbusDaemonItf, SIGNAL(statusChanged(QString, uint)), SIGNAL(accountStatusChanged(QString, uint)) );
    }
}

void MainWindow::onDBusServiceUnregistered(QString iServiceName )
{
    Q_UNUSED(iServiceName);

    if( mDbusDaemonItf )
    {
        // The daemon has just been unregistered, so delete the proxy
        delete mDbusDaemonItf; mDbusDaemonItf = NULL;

        // update the UI to reflect the fact that the daemon is stopped
        ui->daemonStatusText->setText("Stopped");

        emit accountStatusChanged("", 3); // all account status unknown
    }
}

void MainWindow::logFileChanged(const QString &path)
{
    // refresh the content of the log widget
    QFile file(path);

    if ( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
        return;

    QTextStream logTextStream( &file );
    ui->logTextBrowser->setText(logTextStream.readAll());
    file.close();
}

