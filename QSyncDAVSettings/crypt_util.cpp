#include <QString>
#include <QStringList>
/* libC crypt */
#include <crypt.h>
static char DES_crypt_key[64] =
{
    1,0,0,1,0,1,0,0, 1,0,1,1,0,0,1,1, 1,1,0,0,0,1,0,1, 1,0,0,0,0,0,0,1,
    0,0,1,1,0,1,0,0, 1,1,0,0,1,1,1,0, 1,0,1,0,0,1,0,0, 0,0,1,0,1,0,1,1
};

static void DES_decrypt_string( const QString &iEncryptedString,  QString &oDecryptedString )
{
    QStringList lListInputs = iEncryptedString.split("-", QString::SkipEmptyParts);
    int nbNumbers = lListInputs.size();

    for( int currentNumber = 0; currentNumber < nbNumbers; currentNumber += 8 )
    {
        quint16 block[8];
        char txt[64];
        int i = 0, j = 0;

        memset( txt, 0, 64 );

        // process the eight next characters of the input
        for( i = 0; i < 8 ; i++ )
        {
            block[i] = lListInputs.at(currentNumber + i).toUInt(0, 16);
            for ( j = 0; j < 8; j++ )
                txt[i*8+j] = block[i] >> j & 1;
        }

        setkey( DES_crypt_key );
        encrypt( txt, 1 );  // decrypt

        for ( i = 0; i < 8; i++ )
        {
            unsigned char decrypted_char = 0;
            for ( j = 0; j < 8; j++ )
            {
                decrypted_char |= txt[i*8+j] << j;
            }
            oDecryptedString += decrypted_char;
        }
    }
}

void DES_encrypt_string( const QString &iDecryptedString,  QString &oEncryptedString )
{
    int nbChars = iDecryptedString.toLatin1().size();

    for( int currentChar = 0; currentChar < nbChars; currentChar += 8 )
    {
        char txt[64];
        int i = 0, j = 0;

        memset( txt, 0, 64 );

        // process the eight first characters of "input"
        for( i = 0; i+currentChar < nbChars && i < 8 ; i++ )
        {
            char cChar = iDecryptedString.toLatin1().at(i+currentChar);
            for ( j = 0; j < 8; j++ )
                txt[i*8+j] = cChar >> j & 1;
        }

        setkey( DES_crypt_key );
        encrypt( txt, 0 );  // encrypt

        for ( i = 0; i < 8; i++ )
        {
            unsigned char current_letter = 0;
            for ( j = 0; j < 8; j++ )
            {
                current_letter |= txt[i*8+j] << j;
            }
            oEncryptedString += QString().sprintf("%02X-", current_letter);
        }
    }
}

