/******************************************************************************
 *    Copyright 2012 Christophe Chapuis chris.chapuis@gmail.com
 *    Copyright 2011 Juan Carlos Cornejo jc2@paintblack.com
 *
 *    This file is part of QSyncDAV.
 *
 *    QSyncDAV is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    QSyncDAV is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with QSyncDAV.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDBusInterface>

class QFileSystemWatcher;

namespace Ui {
    class MainWindow;
}

#include "AccountFrame.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    enum ScreenOrientation {
        ScreenOrientationLockPortrait,
        ScreenOrientationLockLandscape,
        ScreenOrientationAuto
    };

    explicit MainWindow(QWidget *parent = 0);
    virtual ~MainWindow();

    // Note that this will only have an effect on Symbian and Fremantle.
    void setOrientation(ScreenOrientation orientation);

    void showExpanded();

Q_SIGNALS:
    void accountStatusChanged(QString, uint);

public Q_SLOTS:
    void addAccount();
    void deleteAccount(AccountFrame*);
    void loadConfiguration();
    void saveConfiguration();

private slots:
    void on_actionSave_settings_triggered();
    void on_actionReload_settings_triggered();

    void logFileChanged(const QString &path);

    void onDBusServiceRegistered( QString iServiceName );
    void onDBusServiceUnregistered( QString iServiceName );

    void startAccount(QString iAccountName);
    void stopAccount(QString iAccountName);

private:
    Ui::MainWindow *ui;

    QString mLogFilePath;
    QDBusInterface *mDbusDaemonItf;
    QFileSystemWatcher *mLogWatcher;

    void _addAccount(AccountFrame::AccountProperties iProperties);
};

#endif // MAINWINDOW_H
