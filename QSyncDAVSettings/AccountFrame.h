#ifndef ACCOUNTFRAME_H
#define ACCOUNTFRAME_H

#include <QWidget>

namespace Ui {
class AccountFrame;
}

class AccountFrame : public QWidget
{
    Q_OBJECT
    
public:
    explicit AccountFrame(QWidget *parent, const QString &iAccountName);
    ~AccountFrame();
    
    class AccountProperties {
    public:
        QString accountName;
        QString server;
        QString login;
        QString password;
        QString localDir;
        QString remoteDir;
        qint64  syncInterval;

        AccountProperties() {syncInterval = 10;}
    };

    void getAccountProperties(AccountProperties &oProperties);
    void setAccountProperties(const AccountProperties &iProperties);

    void setAccountIsStarted( bool isStarted );

Q_SIGNALS:
    void start(QString);
    void stop(QString);
    void requestDeletion(AccountFrame*);

public Q_SLOTS:
    void destroyAccount();
    void showFileDialog();
    void startstopAccount();

    void onAccountStatusChange(const QString &iAccountName, uint newStatus);

private:
    Ui::AccountFrame *ui;
    bool mAccountIsStarted;
};

#endif // ACCOUNTFRAME_H
