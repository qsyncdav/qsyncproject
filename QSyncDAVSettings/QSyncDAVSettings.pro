QT += core gui dbus

SOURCES += main.cpp mainwindow.cpp \
    AccountFrame.cpp \
    crypt_util.cpp

HEADERS += mainwindow.h \
    AccountFrame.h

FORMS += mainwindow.ui \
    AccountFrame.ui

LIBS += -lcrypt

DEFINES += QSYNCDAEMON_DBUS_SERVICE=\'\"org.maemo.QSyncDAVDaemon\"\'

maemo5 {
    target.path = /opt/QSyncDAVSettings/bin
    INSTALLS += target
}
